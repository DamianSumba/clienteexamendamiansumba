import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.EmptyBorder;
import javax.ws.rs.core.Response;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class VentanaPrincipal extends JFrame {

	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtPrecio;
	private JTextField txtStock;
	private JTable table;
	
	private DefaultTableModel model;
	private DefaultTableModel model1;

	private List<Producto> lista;
	private List<Carrito>listaC;
	private JTable table1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaPrincipal frame = new VentanaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	
	public void listar() {
		
		model =  new DefaultTableModel();
		model.addColumn("id");
		model.addColumn("nombre");
		model.addColumn("precio");
		model.addColumn("stock");
		GestionProducto gestion = new  GestionProducto();
		lista  =  gestion.listarProducto();
		
		for (Producto p: lista) {
			
			Object[] datos =  new Object[5];
			datos[0] = p.getId();
			datos[1] = p.getNombre();
			datos[2] = p.getPrecio();
			datos[3] = p.getStock();
			
			model.addRow(datos);
		}
		
		table.setModel(model);
	}
	
public void listarCarrito() {
		
		model1 =  new DefaultTableModel();
		model1.addColumn("id");
		model1.addColumn("producto");
		model1.addColumn("cantidad");
		model1.addColumn("total");
		GestionProducto gestion = new  GestionProducto();
		listaC = gestion.listarCarrito();
		
		for (Carrito p: listaC) {
			
			Object[] datos =  new Object[5];
			datos[0] = p.getId();
			datos[1] = p.getProducto().getNombre();
			datos[2] = p.getCantidad();
			datos[3] = p.getTotal();
			
			model1.addRow(datos);
		}
		
		table1.setModel(model1);
	}
	public VentanaPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 664, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(94, 38, 86, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtPrecio = new JTextField();
		txtPrecio.setBounds(94, 87, 86, 20);
		contentPane.add(txtPrecio);
		txtPrecio.setColumns(10);
		
		JLabel Nombre = new JLabel("Nombre:");
		Nombre.setBounds(20, 41, 46, 14);
		contentPane.add(Nombre);
		
		JLabel lblNewLabel_1 = new JLabel("Precio:");
		lblNewLabel_1.setBounds(20, 103, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GestionProducto gestio = new GestionProducto();
				Response r = gestio.insetarProducto(txtNombre.getText(), Double.parseDouble(txtPrecio.getText()),Integer.parseInt(txtStock.getText() ));
				listar();
			}
		});
		btnAgregar.setBounds(94, 204, 89, 23);
		contentPane.add(btnAgregar);
		
		txtStock = new JTextField();
		txtStock.setBounds(94, 141, 86, 20);
		contentPane.add(txtStock);
		txtStock.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Stock");
		lblNewLabel.setBounds(20, 144, 46, 14);
		contentPane.add(lblNewLabel);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int id =  Integer.parseInt(table.getValueAt(table.getSelectedRow(), 0).toString());
				
				double total = Double.parseDouble(table.getValueAt(table.getSelectedRow(), 2).toString());
				String nombre = table.getValueAt(table.getSelectedRow(),1).toString();
				int stock = Integer.parseInt(table.getValueAt(table.getSelectedRow(), 3).toString());
				Producto p = new Producto();
				p.setId(id);
				p.setNombre(nombre);
				p.setStock(stock);
				p.setPrecio(total);
				Opcion pop = new Opcion(table, model,id,1,total,p);
		        if (SwingUtilities.isRightMouseButton(arg0)) {
		            pop.show(arg0.getComponent(), arg0.getX(), arg0.getY());
		        }
			}
		});
		
		table.setBounds(241, 24, 283, 149);
		contentPane.add(table);
		
		table1 = new JTable();
		table1.setBounds(271, 347, 218, -103);
		contentPane.add(table1);
		
		JButton btnNewButton = new JButton("Actualizar Carrito");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listarCarrito();
			}
		});
		btnNewButton.setBounds(244, 219, 166, 23);
		contentPane.add(btnNewButton);
		listar();
		//listarCarrito();
	}
}

class Opcion extends JPopupMenu {



    public Opcion(JTable jTable, DefaultTableModel model, int id,int cantidad,double total,Producto p) {

        JMenuItem delete = new JMenuItem("Agregar al carrito");
        int filaSeleccionada = jTable.getSelectedRow();
        System.out.println("fila seleccionada: " + filaSeleccionada);
        if (filaSeleccionada >= 0) {

            delete.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    GestionProducto gestionProducto = new GestionProducto();
                    Producto p = new Producto();
                    
                    gestionProducto.insetarCarrito(p, total, cantidad);
                    

                    JOptionPane.showMessageDialog(null, "Producto Agregado");
                }

            });

            add(delete);

        } else {

            JOptionPane.showMessageDialog(null, "Seleccione una Fila");

        }
    }

}
