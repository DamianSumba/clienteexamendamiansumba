

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

public class GestionProducto {
	
	private String service = "http://localhost:8080/ServidorDamianSumba/ws/shop/insert";
	private String service2 = "http://localhost:8080/ServidorDamianSumba/ws/shop/listar";
	private String service3 = "http://localhost:8080/ServidorDamianSumba/ws/shop/insertc";
	private String service4 = "http://localhost:8080/ServidorDamianSumba/ws/shop/listarc";
	
	public Response insetarProducto(String nombre,  double precio,int stock) {
		
		Producto p = new Producto();
		p.setNombre(nombre);
		p.setPrecio(precio);
		p.setStock(stock);
		System.out.println("entro al insert");
		Client client = ClientBuilder.newClient();
		System.out.println("entro al insert2");

		WebTarget target = client.target(service);
		System.out.println("entro al insert3");
		Response respuesta = target.request().post(Entity.json(p),Response.class);
		System.out.println("entro al insert4");
		return respuesta;
		
		
			
			
	}
	
	public List<Producto> listarProducto() {
		
		Client client = ClientBuilder.newClient();

		WebTarget target = client.target(service2);
		
		List<Producto> productos = target.request().get(new GenericType<List<Producto>>() {});
		client.close();
		return productos;
		
	}
	
	public List<Carrito> listarCarrito() {
		
		Client client = ClientBuilder.newClient();

		WebTarget target = client.target(service4);
		
		List<Carrito> productos = target.request().get(new GenericType<List<Carrito>>() {});
		client.close();
		return productos;
		
	}
	
	public Response insetarCarrito(Producto producto,  double total,int cantidad) {
		
		Carrito c  =  new Carrito();
		c.setProducto(producto);
		c.setCantidad(cantidad);
		c.setTotal(total);
		System.out.println("entro al insert");
		Client client = ClientBuilder.newClient();
		System.out.println("entro al insert2");

		WebTarget target = client.target(service3);
		System.out.println("entro al insert3");
		Response respuesta = target.request().post(Entity.json(c),Response.class);
		System.out.println("entro al insert4");
		return respuesta;
		
		
			
			
	}	


}
